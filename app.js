var express = require('express');
var app = express();

var requestTime = function (req, res, next) {
    req.requestTime = Date.now();
    next();
};

app.use(requestTime);

app.get('/', function (req, res) {
    var text = "Requested at: " + req.requestTime;
    res.send(text);
});

module.exports = app;